TARGET = libAL.a
OBJS = src/core.o src/buffer.o src/listener.o src/source.o src/context.o src/alut.o src/platform.o src/math.o

KOS_CFLAGS += -ffast-math -Os -Iinclude -std=c99

defaultall: $(OBJS) linklib samples

.PHONY: samples
samples: 
	$(KOS_MAKE) -C samples

include ${KOS_PORTS}/scripts/lib.mk
