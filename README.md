# ALdc 2.0 (WIP)

ALdc is an OpenAL implementation for the SEGA Dreamcast

ALdc 1.0 was based on the most excellent MojoAL, and the port was achieved by reimplementing the parts of SDL that MojoAL depended on. ALdc 2.0
is a ground-up implementation designed to leverage as much as possible of the Dreamcast AICA SPU.

