#include <float.h>
#include <stdio.h>

#include "private.h"


void _alInitBufferQueueIterator(BufferQueueIterator* iterator) {
    iterator->entry = NULL;
    iterator->sample_count = 0;
    iterator->last_sample = 0;

    iterator->channels[0] = -1;
    iterator->channels[1] = -1;
}

void _alInitBufferQueueEntry(BufferQueueEntry* entry, Buffer* buffer) {
    entry->next = NULL;
    entry->buffer = buffer;
}

void _alInitSource(Source* source) {
    _alInitTransform(&source->transform);

    source->name = 0;
    source->is_dead = false;
    source->reference_distance = 1.0f;
    source->rolloff_factor = 1.0f;
    source->max_distance = FLT_MAX;
    source->gain = 1.0f;
    source->pitch = 1.0f;
    source->state = AL_INITIAL;
    source->type = AL_UNDETERMINED;
    source->looping = AL_FALSE;
    source->relative = AL_FALSE;

    source->buffers_processed = 0;
    source->buffer_queue_head = NULL;
    source->buffer_queue_tail = NULL;

    _alInitBufferQueueIterator(&source->buffer_queue_iterator);

    pthread_mutex_init(&source->mutex, NULL);
}

static void _alSourceResetQueue(Source* source) {
    BufferQueueEntry* entry = source->buffer_queue_head;
    while(entry) {
        BufferQueueEntry* next = entry->next;
        free(entry);
        entry = next;
    }

    source->buffer_queue_head = source->buffer_queue_tail = NULL;
}

Source* _alSource(ALuint source) {
    Context* ctx = _alContext();
    if(!ctx) {
        return NULL;
    }

    assert(source > 0);
    assert(source < MAX_SOURCES_PER_CONTEXT);

    Source* s = &ctx->sources[source - 1];
    if(s->is_dead) {
        return NULL;
    }

    return s;
}

AL_API void AL_APIENTRY alGenSources(ALsizei n, ALuint *sources) {
    Context* ctx = _alContext();
    if(!ctx) {
        _alSetError(AL_INVALID_OPERATION, "No current context");
        return;
    }

    ALsizei available = MAX_SOURCES_PER_CONTEXT - ctx->source_count;
    if(n > available) {
        _alSetError(AL_OUT_OF_MEMORY, "Not enough sources");
        return;
    }

    Source* s = ctx->sources;
    ALsizei c = 0;
    for(int i = 0; i < MAX_SOURCES_PER_CONTEXT; ++i) {
        {
            SCOPED_LOCK(&s[i].mutex);

            if(s[i].is_dead) {
                sources[c++] = i + 1;
                s[i].is_dead = false;
                s[i].name = i + 1;

                if(c == n) {
                    break;
                }
            }
        }
    }

    assert(c == n);
}

AL_API void AL_APIENTRY alDeleteSources(ALsizei n, const ALuint *sources) {
    Context* ctx = _alContext();
    if(!ctx) {
        _alSetError(AL_INVALID_OPERATION, "No current context");
        return;
    }

    for(int i = 0; i < n; ++i) {
        Source* s = &ctx->sources[sources[i] - 1];
        if(s->is_dead) {
            _alSetError(AL_INVALID_NAME, "Tried to delete an invalid source");
        }
    }

    for(int i = 0; i < n; ++i) {
        Source* s = &ctx->sources[sources[i] - 1];
        s->is_dead = true;
        pthread_mutex_destroy(&s->mutex);
    }
}

AL_API ALboolean AL_APIENTRY alIsSource(ALuint source) {
    return (_alSource(source)) ? AL_FALSE : AL_TRUE;
}

AL_API void AL_APIENTRY alSourcef(ALuint source, ALenum param, ALfloat value) {
    const ALenum VALID [] = {AL_GAIN, AL_REFERENCE_DISTANCE, AL_PITCH, 0};

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);

    SCOPED_LOCK(&obj->mutex);

    if(obj) {
        switch(param) {
            case AL_PITCH:
                obj->pitch = value;
                return;
            case AL_GAIN:
                obj->gain = value;
                return;
            case AL_REFERENCE_DISTANCE:
                obj->reference_distance = value;
                return;
            case AL_ROLLOFF_FACTOR:
                obj->rolloff_factor = value;
                return;
            case AL_MAX_DISTANCE:
                obj->max_distance = value;
            default:
                break;
        }
    }

    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSource3f(ALuint source, ALenum param, ALfloat value1, ALfloat value2, ALfloat value3) {
    const ALenum VALID [] = {AL_POSITION, AL_VELOCITY, 0};

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);

    if(obj) {
        switch(param) {
            case AL_POSITION:
                obj->transform.position.x = value1;
                obj->transform.position.y = value2;
                obj->transform.position.z = value3;
                return;
            break;
            case AL_VELOCITY:
                obj->transform.velocity.x = value1;
                obj->transform.velocity.y = value2;
                obj->transform.velocity.z = value3;
                return;
            break;
            default:
                break;
        }
    }

    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcefv(ALuint source, ALenum param, const ALfloat *values) {
    const ALenum VALID [] = {AL_POSITION, AL_VELOCITY, 0};

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);

    if(obj) {
        switch(param) {
            case AL_POSITION:
                obj->transform.position.x = values[0];
                obj->transform.position.y = values[1];
                obj->transform.position.z = values[2];
                return;
            break;
            case AL_VELOCITY:
                obj->transform.velocity.x = values[0];
                obj->transform.velocity.y = values[1];
                obj->transform.velocity.z = values[2];
                return;
            break;
            default:
                break;
        }
    }

    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcei(ALuint source, ALenum param, ALint value) {

    Source* obj = _alSource(source);
    if(!obj) {
        _alSetError(AL_INVALID_NAME, "Invalid source name");
        return;
    }

    if(param == AL_LOOPING) {
        obj->looping = (value) ? AL_TRUE : AL_FALSE;
        return;
    } else if(param == AL_BUFFER) {
        if(obj->state == AL_PLAYING || obj->state == AL_PAUSED) {
            _alSetError(AL_INVALID_OPERATION, "Tried to set the buffer on a playing source");
            return;
        }

        _alSourceResetQueue(obj);

        Buffer* buffer = (value) ? _alBuffer(value) : &_alContext()->null_buffer;

        if(!buffer) {
            _alSetError(AL_INVALID_NAME, "Non existent buffer");
            return;
        }
        obj->buffer_queue_head = (BufferQueueEntry*) malloc(sizeof(BufferQueueEntry));
        _alInitBufferQueueEntry(obj->buffer_queue_head, buffer);

        obj->buffer_queue_tail = obj->buffer_queue_head;
        obj->buffer_queue_iterator.entry = obj->buffer_queue_head;

        obj->type = (value) ? AL_STATIC : AL_UNDETERMINED;
        return;
    } else if(param == AL_SOURCE_RELATIVE) {
        obj->relative = (value) ? true : false;
        return;
    }

    _alSetError(AL_INVALID_OPERATION, "Not implemented");
}

AL_API void AL_APIENTRY alSource3i(ALuint source, ALenum param, ALint value1, ALint value2, ALint value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceiv(ALuint source, ALenum param, const ALint *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourcef(ALuint source, ALenum param, ALfloat *value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSource3f(ALuint source, ALenum param, ALfloat *value1, ALfloat *value2, ALfloat *value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourcefv(ALuint source, ALenum param, ALfloat *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourcei(ALuint source,  ALenum param, ALint *value) {
    const ALenum VALID [] = {AL_SOURCE_STATE, AL_BUFFERS_PROCESSED, 0};
    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);
    if(obj) {
        switch(param) {
            case AL_SOURCE_STATE:
                *value = (ALint) obj->state;
                return;
            break;
            case AL_BUFFERS_PROCESSED:
                /* We don't return the processed buffers if we're looping */
                *value = (obj->looping) ? 0 : obj->buffers_processed;
                return;
            break;
            default:
                break;
        }
    }

    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSource3i(ALuint source, ALenum param, ALint *value1, ALint *value2, ALint *value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourceiv(ALuint source,  ALenum param, ALint *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePlayv(ALsizei n, const ALuint *sources) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceStopv(ALsizei n, const ALuint *sources) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceRewindv(ALsizei n, const ALuint *sources) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePausev(ALsizei n, const ALuint *sources) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePlay(ALuint source) {
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        obj->buffer_queue_iterator.entry = obj->buffer_queue_head;
        obj->state = AL_PLAYING;
        obj->buffers_processed = 0;
    }
}

AL_API void AL_APIENTRY alSourceStop(ALuint source) {
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        if(obj->state == AL_INITIAL) {
            return;
        }

        obj->state = AL_STOPPED;
        obj->buffer_queue_iterator.entry = NULL;
    }
}

AL_API void AL_APIENTRY alSourceRewind(ALuint source) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePause(ALuint source) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceQueueBuffers(ALuint source, ALsizei nb, const ALuint *buffers) {
    Context* ctx = _alContext();
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        for(ALsizei i = 0; i < nb; ++i) {
            BufferQueueEntry* entry = (BufferQueueEntry*) malloc(sizeof(BufferQueueEntry));
            entry->next = NULL;

            if(buffers[i] == 0) {
                // The NULL buffer is a thing
                entry->buffer = &ctx->null_buffer;
            } else {
                entry->buffer = _alBuffer(buffers[i]);
                assert(entry->buffer);

                if(obj->buffer_queue_head) {
                    if(obj->buffer_queue_head->buffer->format != entry->buffer->format) {
                        free(entry);
                        _alSetError(AL_INVALID_OPERATION, "Format mismatch when queuing buffers");
                        return;
                    }
                }
            }

            /* If we had a tail, we append this to the end of the list */
            if(obj->buffer_queue_tail) {
                assert(obj->buffer_queue_head);

                obj->buffer_queue_tail->next = entry;
            } else {
                /* Set the head if this was the first thing in the queue */
                obj->buffer_queue_head = entry;
            }

            obj->buffer_queue_tail = entry;
        }
    } else {
        _alSetError(AL_INVALID_NAME, "Invalid source specified");
    }
}

AL_API void AL_APIENTRY alSourceUnqueueBuffers(ALuint source, ALsizei nb, ALuint *buffers) {
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        if(obj->buffers_processed < nb) {
            _alSetError(AL_INVALID_VALUE, "Requested to unqueue more buffers than processed");
            return;
        }

        for(ALsizei i = 0; i < nb; ++i) {
            BufferQueueEntry* entry = obj->buffer_queue_head;

            assert(entry != obj->buffer_queue_iterator.entry);

            /* Get the buffer that the head is pointing at */
            buffers[i] = entry->buffer->name;
            obj->buffers_processed--;

            /* Shift the head of the queue */
            obj->buffer_queue_head = entry->next;
            free(entry);

            /* If we cleared the buffer queue, then we need to wipe
             * the tail */
            if(!obj->buffer_queue_head) {
                obj->buffer_queue_tail = NULL;
                obj->buffer_queue_iterator.entry = NULL;
            }
        }
    } else {
        _alSetError(AL_INVALID_NAME, "Invalid source specified");
    }
}
