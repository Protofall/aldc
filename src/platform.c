#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include "platform.h"

#ifdef _arch_dreamcast

#include "aica_cmd_iface.h"

#define SPU_RAM_BASE            0xa0800000


#define SPU_CHANNEL_BASE        0xA0700000
#define SPU_CHANNEL_SIZE        32 * 4

#define dc_snd_base ((volatile unsigned char *) SPU_CHANNEL_BASE)

#define SNDREG32A(x) ((volatile unsigned long *)(dc_snd_base + (x)))
#define SNDREG32(x) (*SNDREG32A(x))
#define SNDREG8A(x) (dc_snd_base + (x))
#define SNDREG8(x) (*SNDREG8A(x))

void init_spu() {
    snd_init();
    snd_sh4_to_aica_start();
}

void play_channel(int channel, int addr, int freq, int bits, int size) {
    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    if(size >= 65535) size = 65534;

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_START;
    chan->base = addr;
    chan->type = (bits == 16) ? AICA_SM_16BIT : AICA_SM_8BIT;
    chan->length = size;
    chan->loop = 0;
    chan->loopstart = 0;
    chan->loopend = size;
    chan->freq = freq;
    chan->vol = 254;
    chan->pos = 0;
    chan->pan = 128;
    snd_sh4_to_aica(tmp, cmd->size);
}

void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size) {
    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    if(size >= 65535) size = 65534;

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = c0;
    chan->cmd = AICA_CH_CMD_START | AICA_CH_START_DELAY;
    chan->base = addr0;
    chan->type = (bits == 16) ? AICA_SM_16BIT : AICA_SM_8BIT;
    chan->length = size;
    chan->loop = 0;
    chan->loopstart = 0;
    chan->loopend = size;
    chan->freq = freq;
    chan->vol = 254;
    chan->pos = 0;
    chan->pan = 0;
    snd_sh4_to_aica(tmp, cmd->size);

    cmd->cmd_id = c1;
    chan->base = addr1;
    chan->pan = 255;
    snd_sh4_to_aica(tmp, cmd->size);

    cmd->cmd_id = (1 << c0) | (1 << c1);
    chan->cmd = AICA_CH_CMD_START | AICA_CH_START_SYNC;
    snd_sh4_to_aica(tmp, cmd->size);
}

void stop_channel(int channel) {
    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_STOP;
    snd_sh4_to_aica(tmp, cmd->size);
}

int channel_position(int channel) {
    assert(channel > -1);

    return g2_read_32(SPU_RAM_BASE + AICA_CHANNEL(channel) + offsetof(aica_channel_t, pos));
}

void pan_channel(int channel, float pan) {
    int final_pan = (128.0f * pan) + 128.0f;

    if(channel < 0) return;

    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_UPDATE | AICA_CH_UPDATE_SET_PAN;
    chan->pan = final_pan;
    snd_sh4_to_aica(tmp, cmd->size);
}

void amp_channel(int channel, float v) {
    if(channel < 0) return;

    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_UPDATE | AICA_CH_UPDATE_SET_VOL;
    chan->vol = 255.0f * v;
    snd_sh4_to_aica(tmp, cmd->size);
}

#else

/* Really stupid emulation of what the DC is doing, except the
 * DC is actually doing some sensible memory allocation rather than
 * this! */

#define CHUNK_COUNT 1024
static uint8_t* CHUNKS[CHUNK_COUNT] = {0};

int alloc_spu_ram(int size) {
    for(int i = 0; i < 1024; ++i) {
        if(!CHUNKS[i]) {
            CHUNKS[i] = (uint8_t*) malloc(size);
            return i + 1;
        }
    }

    /* Out of "memory"! */
    return 0;
}

void free_spu_ram(int addr) {
    free(CHUNKS[addr - 1]);
    CHUNKS[addr - 1] = NULL;
}

void copy_to_spu(int32_t addr, void* src, int size) {
    memcpy(CHUNKS[addr - 1], src, size);
}

void init_spu() {

}

static uint64_t channels = 0;

int alloc_spu_channel() {
    int i = 0;
    while((channels & (1 << i))) {
        if(++i == 64) {
            fprintf(stderr, "Channel overflow\n");
        }
    }

    channels |= (1 << i);
    return i;
}

void release_spu_channel(int c) {
    channels &= ~(1 << c);
}

void play_channel(int channel, int addr, int freq, int bits, int size) {

}

void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size) {

}

int channel_position(int channel) {
    return 0;
}

void stop_channel(int channel) {

}

void pan_channel(int channel, float pan) {

}

void amp_channel(int channel, float v) {

}

uint64_t time_in_us() {
    return 0; // FIXME:
}

#endif
