#include <stdio.h>
#include <stdbool.h>

#include "private.h"

static ALenum CURRENT_ERROR = AL_NO_ERROR;
static ALenum DISTANCE_MODEL = AL_INVERSE_DISTANCE_CLAMPED;

ALboolean _alSetError(ALenum error, const ALchar* msg) {
    if(CURRENT_ERROR == AL_NO_ERROR) {
        CURRENT_ERROR = error;
        fprintf(stderr, "%s\n", msg);
        return AL_TRUE;
    }

    return AL_FALSE;
}

ALenum _alDistanceModel() {
    return DISTANCE_MODEL;
}

AL_API void AL_APIENTRY alDopplerFactor(ALfloat value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alDopplerVelocity(ALfloat value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSpeedOfSound(ALfloat value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alDistanceModel(ALenum distanceModel) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alEnable(ALenum capability) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alDisable(ALenum capability) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API ALboolean AL_APIENTRY alIsEnabled(ALenum capability) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API const ALchar* AL_APIENTRY alGetString(ALenum param) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return "";
}

AL_API void AL_APIENTRY alGetBooleanv(ALenum param, ALboolean *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetIntegerv(ALenum param, ALint *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetFloatv(ALenum param, ALfloat *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetDoublev(ALenum param, ALdouble *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API ALboolean AL_APIENTRY alGetBoolean(ALenum param) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API ALint AL_APIENTRY alGetInteger(ALenum param) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return 0;
}

AL_API ALfloat AL_APIENTRY alGetFloat(ALenum param) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return 0.0f;
}

AL_API ALdouble AL_APIENTRY alGetDouble(ALenum param) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return 0.0;
}

AL_API ALenum AL_APIENTRY alGetError(void) {
    ALenum ret = CURRENT_ERROR;
    CURRENT_ERROR = AL_NO_ERROR;
    return ret;
}

AL_API ALboolean AL_APIENTRY alIsExtensionPresent(const ALchar *extname) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API void* AL_APIENTRY alGetProcAddress(const ALchar *fname) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return NULL;
}

AL_API ALenum AL_APIENTRY alGetEnumValue(const ALchar *ename) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return (ALenum) 0;
}


/* Helpers */

void _alInitTransform(Transform* transform) {
    _alInitVec3(&transform->position);
    _alInitVec3(&transform->velocity);
}

bool _alIsValidEnum(const ALenum* valid, ALenum test) {
    const ALenum* it = valid;
    while(*it) {
        const ALenum value = *it;
        if(value == test) {
            return true;
        }

        it++;
    }

    return false;
}
