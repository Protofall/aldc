#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <pthread.h>

#include "AL/al.h"

#include "math.h"

#define MAX_CONTEXTS 2

/* This is the max number of sources that can be generated, but
 * the max number that can be played concurrently is at most 64
 * (if all mono) */
#define MAX_SOURCES_PER_CONTEXT 256
#define MAX_BUFFERS_PER_CONTEXT 1024

ALboolean _alSetError(ALenum error, const ALchar* msg);
bool _alIsValidEnum(const ALenum* valid, ALenum test);

typedef struct {
    Vec3 position;
    Vec3 velocity;
} Transform;

void _alInitTransform(Transform* transform);

typedef struct Buffer {
    ALuint name;
    bool is_dead;

    ALenum format;
    ALsizei data_size;

    /* The DC SPU works with address offsets from some base address
     * rather than pointers. That's what data is */
    ALsizei data;

    ALsizei freq;

} Buffer;

void _alInitBuffer(Buffer* buffer);

struct _BufferQueueEntry;

typedef struct _BufferQueueEntry {
    Buffer* buffer;
    struct _BufferQueueEntry* next;
} BufferQueueEntry;

typedef struct _BufferQueueIterator {
    /* When we start playing a buffer, we set the timestamp
     * that the buffer should complete, then use that to change
     * the state. */

    BufferQueueEntry* entry;

    /* Mono sources will only use one channel, if channels[0] == -1
     * then no channels have been allocated */
    int channels[2];

    uint32_t sample_count;
    uint32_t last_sample;
    uint64_t end_time;
} BufferQueueIterator;

typedef struct {
    Transform transform;

    ALuint name;
    bool is_dead;

    float reference_distance;
    float rolloff_factor;
    float max_distance;
    float gain;
    float pitch;
    ALboolean looping;

    ALenum state;
    ALenum type;
    ALboolean relative;

    ALint buffers_processed;
    BufferQueueEntry* buffer_queue_head;
    BufferQueueEntry* buffer_queue_tail;
    BufferQueueIterator buffer_queue_iterator;

    pthread_mutex_t mutex;
} Source;

void _alInitSource(Source* source);

typedef struct {
    Transform transform;

    Vec3 right;
    Vec3 up;
    Vec3 forward;
} Listener;

void _alInitListener(Listener* listener);

struct _ALCdevice_struct;

typedef struct _ALCcontext_struct {
    struct _ALCdevice_struct* device;

    Buffer null_buffer;
    Buffer buffers[MAX_BUFFERS_PER_CONTEXT];
    Source sources[MAX_SOURCES_PER_CONTEXT];

    int buffer_count;
    int source_count;

    bool is_dead;
} ALCcontext_struct;

typedef struct _ALCdevice_struct {
    const char* name;
    struct _ALCcontext_struct contexts[MAX_CONTEXTS];

    bool is_open;

    struct _ALCdevice_struct* next;
} ALCdevice_struct;

typedef ALCdevice_struct Device;
typedef ALCcontext_struct Context;

void _alInitContext(Context* context);
void _alInitDevice(Device* device, const char* name);

Listener* _alListener();
Buffer* _alBuffer(ALuint buffer);
Context* _alContext();
Source* _alSource(ALuint source);

ALenum _alDistanceModel();


/* RAII locking */

typedef struct {
    pthread_mutex_t* mutex;
} ScopedLock ;

inline static void unlock(ScopedLock* mutex) {
    pthread_mutex_unlock(mutex->mutex);
}

#define SCOPED_LOCK(mutex) \
    ScopedLock _lock __attribute__((cleanup(unlock))) = {mutex}; \
    (void) _lock; \
    pthread_mutex_lock(mutex)
