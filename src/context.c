#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "private.h"
#include "platform.h"
#include "math.h"

#include "../include/AL/alc.h"

#define _THREAD_DEBUG 0


static const int SLEEP_TIME = 1000;
static Context* CURRENT = NULL;
static pthread_t DEVICE_THREAD;

static Device DEVICES [] = {
    {"Yamaha ARM7 (67 Mhz) Analog Output", {}, false, NULL}
};

static const uint32_t NUM_DEVICES = sizeof(DEVICES) / sizeof(Device);

static bool INITIALIZED = false;

static void _alInitDevices() {
    if(INITIALIZED) {
        return;
    }

    for(int i = 0; i < NUM_DEVICES; ++i) {
        DEVICES[i].is_open = false;
    }

    init_spu();
    _alInitListener(_alListener());


    INITIALIZED = true;
}

static char DEVICE_STRING[1024];

static float _alCalculatePanning(Source* s) {
    /* Given a source, returns a value between -1.0f and 1.0f */

    Listener* l = _alListener();
    /* This would be -1 if the sound was directly left, +1 if directly
     * right. 0 if front or back */
    Vec3 dir;
    _alVec3Subtract(&s->transform.position, &l->transform.position, &dir);
    _alVec3Normalize(&dir);

    return _alVec3Dot(&l->right, &dir);
}

static float _alCalculateGain(Source* s) {
    ALenum distance_model = _alDistanceModel();
    Listener* l = _alListener();

    Vec3 dir;
    _alVec3Subtract(&s->transform.position, &l->transform.position, &dir);

    float gain = 1.0f;
    float distance = _alVec3Length(&dir);
    float ref_distance = s->reference_distance;
    float roll_off = s->rolloff_factor;

#define CLAMP_DISTANCE() \
    distance = (distance > ref_distance) ? distance : ref_distance; \
    distance = (distance < s->max_distance) ? distance : s->max_distance

    switch(distance_model) {
        case AL_INVERSE_DISTANCE_CLAMPED:
            CLAMP_DISTANCE();  /* Explicit fallthrough */
        case AL_INVERSE_DISTANCE:
            // FIXME: Fast divide
            gain = ref_distance / (ref_distance + roll_off) * (distance - ref_distance);
        break;
        case AL_LINEAR_DISTANCE_CLAMPED:
            CLAMP_DISTANCE(); /* Explicit fallthrough */
        case AL_LINEAR_DISTANCE:
            // FIXME: Implement
        break;
        case AL_EXPONENT_DISTANCE_CLAMPED:
            CLAMP_DISTANCE();  /* Explicit fallthrough */
        case AL_EXPONENT_DISTANCE:
            // FIXME: Implement
        break;
        case AL_NONE:
        default:
             break;
    }

    return gain;
}

static void _alPlayEntry(BufferQueueIterator* iterator) {
    Buffer* buffer = iterator->entry->buffer;
    assert(buffer);

    ALenum format = buffer->format;

    uint8_t bits = (
        buffer->format == AL_FORMAT_MONO16 ||
        buffer->format == AL_FORMAT_STEREO16
    ) ? 16 : 8;

    uint8_t channels = (
        format == AL_FORMAT_STEREO8 ||
        format == AL_FORMAT_STEREO16
    ) ? 2 : 1;

    uint32_t bytes = bits / 8;
    uint32_t samples = buffer->data_size / channels / bytes;

    if(channels == 2) {
        /* Two channels */
        iterator->channels[0] = alloc_spu_channel();
        iterator->channels[1] = alloc_spu_channel();

        assert(iterator->channels[0] > -1);
        assert(iterator->channels[1] > -1);

        play_channels(
            iterator->channels[0],
            iterator->channels[1],
            buffer->data,
            buffer->data + (buffer->data_size / 2),
            buffer->freq,
            bits,
            samples
        );
    } else {
        /* Single channel */
        iterator->channels[0] = alloc_spu_channel();
        iterator->channels[1] = -1;

        assert(iterator->channels[0] > -1);

        play_channel(
            iterator->channels[0],
            buffer->data,
            buffer->freq,
            bits,
            samples
        );
#if _THREAD_DEBUG
        printf(
            "Playing channel: %d (%d)\n",
            iterator->channels[0], samples
        );
#endif
    }

    iterator->sample_count = samples;
    iterator->last_sample = 0;

    uint32_t duration = (
        ((float) samples) / ((float) buffer->freq)
    ) * 1000000;

    iterator->end_time = time_in_us() + duration;
}

static bool _updateSamplePos(BufferQueueIterator* iterator) {
    /*
     * Update the last_sample on the iterator and return true
     * if we just hit the end */

    uint32_t pos = channel_position(iterator->channels[0]);

    if(iterator->channels[1] > -1) {
        uint32_t pos2 = channel_position(iterator->channels[1]);
        pos = (pos < pos2) ? pos : pos2;
    }

    int fudge = SLEEP_TIME / 2;
    bool ret = (iterator->end_time + fudge) <= time_in_us();

    /*
    bool ret = (
        (pos == iterator->sample_count) ||
        (pos == 0 && iterator->last_sample > 0)
    ); */

    iterator->last_sample = pos;

    return ret;
}

static void* _alDeviceThread(void* dev) {
    /* Where the magic happens */

    Device* device = (Device*) dev;

    printf("OpenAL: Starting device thread\n");

    while(device->is_open) {
        for(int i = 0; i < MAX_CONTEXTS; ++i) {
            Context* ctx = &device->contexts[i];
            if(ctx->is_dead) {
                continue;
            }

            usleep(SLEEP_TIME);

            for(int j = 0; j < MAX_SOURCES_PER_CONTEXT; ++j) {
                Source* s = &ctx->sources[j];
                SCOPED_LOCK(&s->mutex);

                if(s->is_dead) {
                    continue;
                }

                BufferQueueIterator* iterator = &s->buffer_queue_iterator;
                Buffer* b = (iterator->entry) ? iterator->entry->buffer : NULL;

                if(s->state == AL_PLAYING) {
                    if(iterator->channels[0] == -1) {
                        /* No channels allocated, but we're playing,
                         * so let's begin */
                        _alPlayEntry(iterator);
                    }
                }

                bool buffer_finished = false;

                if(s->state == AL_PLAYING) {
                    buffer_finished = _updateSamplePos(iterator);

                    assert(b);

                    bool is_mono = (
                        b->format == AL_FORMAT_MONO8 ||
                        b->format == AL_FORMAT_MONO16
                    );

                    if(s->state == AL_PLAYING && is_mono) {
                        /* Still playing? Let's do some positional things! Although
                         * only if it's a mono buffer that's playing */

                        float pan = _alCalculatePanning(s);
                        if(iterator->channels[0] > -1) pan_channel(iterator->channels[0], pan);
                        if(iterator->channels[1] > -1) pan_channel(iterator->channels[1], pan);

                        float gain = _alCalculateGain(s);
                        if(iterator->channels[0] > -1) amp_channel(iterator->channels[0], gain);
                        if(iterator->channels[1] > -1) amp_channel(iterator->channels[1], gain);
                    }
                } else if(s->state == AL_STOPPED && iterator->channels[0] > -1) {
                    buffer_finished = true;
                }

                if(buffer_finished) {
                    /* We've reached the end of the buffer! */
                    /* Release the channels */

                    for(int i = 0; i < 2; ++i) {
                        if(iterator->channels[i] > -1) {
                            stop_channel(iterator->channels[i]);
                            release_spu_channel(iterator->channels[i]);
                            iterator->channels[i] = -1;
                        }
                    }

                    if(s->state == AL_PLAYING) {
                        /* If we're playing, move to the next buffer */
                        assert(iterator->entry);

                        iterator->entry = iterator->entry->next;
                        s->buffers_processed++;

                        /* We reached the end, so mark as stopped, or loop */
                        if(!iterator->entry) {
                            s->state = AL_STOPPED;
                            if(s->looping) {
                                printf("Looping!!\n");
                                s->state = AL_PLAYING;
                                s->buffer_queue_iterator.entry = s->buffer_queue_head;
                                s->buffers_processed = 0;

                                assert(s->buffer_queue_iterator.entry);
                            }
                        }
                    }
                }
            }
        }
    }

    printf("OpenAL: Stopping device thread\n");
    return NULL;
}

void _alInitContext(Context* context) {
    _alInitBuffer(&context->null_buffer);

    context->device = NULL;
    context->is_dead = true;
    context->buffer_count = 0;
    context->source_count = 0;

    for(int i = 0; i < MAX_SOURCES_PER_CONTEXT; ++i) {
        _alInitSource(&context->sources[i]);
        context->sources[i].is_dead = true;
    }

    for(int i = 0; i < MAX_BUFFERS_PER_CONTEXT; ++i) {
        _alInitBuffer(&context->buffers[i]);
        context->buffers[i].is_dead = true;
    }
}

void _alInitDevice(Device* device, const char* name) {
    device->name = name;
    device->is_open = false;
}

AL_API ALCcontext* AL_APIENTRY alcCreateContext(ALCdevice *device, const ALCint* attrlist) {
    if(attrlist) {
        _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    }

    Context* ret = NULL;
    Device* dev = (Device*) device;

    for(int i = 0; i < MAX_CONTEXTS; ++i) {
        if(dev->contexts[i].is_dead) {
            ret = &dev->contexts[i];
            _alInitContext(ret);
            ret->is_dead = false;
            break;
        }
    }

    if(!ret) {
        _alSetError(ALC_INVALID_VALUE, "Out of contexts");
    }

    return (ALCcontext*) ret;
}

AL_API ALCboolean AL_APIENTRY alcMakeContextCurrent(ALCcontext *context) {
    CURRENT = (Context*) context;
    return AL_TRUE;
}

AL_API void AL_APIENTRY alcProcessContext(ALCcontext *context) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcSuspendContext(ALCcontext *context) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcDestroyContext(ALCcontext *context) {
    if(context == (ALCcontext*) CURRENT) {
        _alSetError(AL_INVALID_OPERATION, "Tried to delete the current context");
        return;
    }

    ((Context*) context)->is_dead = true;
}

AL_API ALCcontext* AL_APIENTRY alcGetCurrentContext(void) {
    return (ALCcontext*) CURRENT;
}

AL_API ALCdevice* AL_APIENTRY alcGetContextsDevice(ALCcontext *context) {
    if(!context) {
        _alSetError(AL_INVALID_OPERATION, "Context was NULL");
        return NULL;
    }

    return (ALCdevice*) ((Context*) context)->device;
}

AL_API ALCdevice* AL_APIENTRY alcOpenDevice(const ALCchar *devicename) {
    _alInitDevices();

    Device* to_open = NULL;

    if(devicename == NULL || strlen(devicename) == 0) {
        to_open = &DEVICES[0];
    } else {
        for(int i = 0; i < NUM_DEVICES; ++i) {
            Device* dev = &DEVICES[i];
            if(strcmp(dev->name, devicename) == 0) {
                to_open = dev;
                break;
            }
        }
    }

    assert(to_open);

    if(to_open->is_open) {
        fprintf(stderr, "Already opened device (%s)???\n", to_open->name);
        return NULL;
    }

    to_open->is_open = true;

    for(int i = 0; i < MAX_CONTEXTS; ++i) {
        _alInitContext(&to_open->contexts[i]);
    }

    pthread_create(&DEVICE_THREAD, NULL, &_alDeviceThread, (void*) to_open);

    return (ALCdevice*) to_open;
}

AL_API ALCboolean AL_APIENTRY alcCloseDevice(ALCdevice *device) {
    Device* dev = (Device*) device;

    /* Make sure all buffers were destroyed */
    for(int i = 0; i < MAX_CONTEXTS; ++i) {
        Context* ctx = &dev->contexts[i];
        if(!ctx->is_dead) {
            fprintf(stderr, "Couldn't close device with contexts");
            return AL_FALSE;
        }

        if(ctx->buffer_count) {
            return AL_FALSE;
        }
    }

    dev->is_open = false;

    void* ret;
    pthread_join(DEVICE_THREAD, &ret);

    return AL_TRUE;
}

AL_API ALCenum AL_APIENTRY alcGetError(ALCdevice *device) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return (ALCenum) 0;
}

AL_API ALCboolean AL_APIENTRY alcIsExtensionPresent(ALCdevice *device, const ALCchar *extname) {
    if(strcmp(extname, "ALC_ENUMERATION_EXT") == 0) {
        return AL_TRUE;
    }

    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API void* AL_APIENTRY alcGetProcAddress(ALCdevice *device, const ALCchar *funcname) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return NULL;
}

AL_API ALCenum AL_APIENTRY alcGetEnumValue(ALCdevice *device, const ALCchar *enumname) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return (ALCenum) 0;
}

AL_API const ALCchar* AL_APIENTRY alcGetString(ALCdevice *device, ALCenum param) {
    const ALenum VALID [] = {
        ALC_EXTENSIONS,
        ALC_DEVICE_SPECIFIER,
        ALC_DEFAULT_DEVICE_SPECIFIER,
        0
    };

    printf("Calling getstring\n");

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(AL_INVALID_ENUM, "Invalid enum");
        return "";
    }

    if(param == ALC_DEVICE_SPECIFIER) {
        _alInitDevices();
        sprintf(DEVICE_STRING, "%s%c%c", DEVICES->name, '\0', '\0');
        return DEVICE_STRING;
    } else if(param == ALC_DEFAULT_DEVICE_SPECIFIER) {
        _alInitDevices();
        return DEVICES[0].name;
    } else {
        assert(0 && "Invalid param?");
    }

    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return "";
}

AL_API void AL_APIENTRY alcGetIntegerv(ALCdevice *device, ALCenum param, ALCsizei size, ALCint *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API ALCdevice* AL_APIENTRY alcCaptureOpenDevice(const ALCchar *devicename, ALCuint frequency, ALCenum format, ALCsizei buffersize) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return NULL;
}

AL_API ALCboolean AL_APIENTRY alcCaptureCloseDevice(ALCdevice *device) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API void AL_APIENTRY alcCaptureStart(ALCdevice *device) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcCaptureStop(ALCdevice *device) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcCaptureSamples(ALCdevice *device, ALCvoid *buffer, ALCsizei samples) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

Context* _alContext() {
    return CURRENT;
}
