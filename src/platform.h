#pragma once

/*
 * This file contains shims for all SH4/DC specific functions so that
 * the library can be compiled and run on any platform (even if it doesn't
 * play sound) - this helps debugging.
 */

#include <stdlib.h>
#include <stdint.h>

#ifdef _arch_dreamcast
#include <kos.h>
#include <dc/spu.h>
#include <dc/sound/sound.h>
#include <dc/sound/sfxmgr.h>

#define alloc_spu_ram snd_mem_malloc
#define free_spu_ram snd_mem_free
#define copy_to_spu spu_memload

#define alloc_spu_channel snd_sfx_chn_alloc
#define release_spu_channel snd_sfx_chn_free

void init_spu();
void play_channel(int channel, int addr, int freq, int bits, int size);
void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size);
void stop_channel(int chan);

int channel_position(int channel);

void pan_channel(int channel, float v);
void amp_channel(int channel, float v);

#define time_in_us timer_us_gettime64

#else

int alloc_spu_ram(int size);
void free_spu_ram(int addr);
void copy_to_spu(int32_t addr, void* src, int size);

int alloc_spu_channel();
void release_spu_channel(int c);

void init_spu();

void play_channel(int channel, int addr, int freq, int bits, int size);
void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size);

void stop_channel(int chan);
void pan_channel(int channel, float v);
void amp_channel(int channel, float v);

int channel_position(int channel);

uint64_t time_in_us();

#endif
