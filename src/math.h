#pragma once

typedef struct {
    float x, y, z;
} Vec3;

void _alInitVec3(Vec3* v);

float _alVec3Dot(const Vec3* lhs, const Vec3* rhs);
void _alVec3Subtract(const Vec3* lhs, const Vec3* rhs, Vec3* out);
void _alVec3Normalize(Vec3* v);
float _alVec3Length(const Vec3* v);
