#include <stdio.h>

#include "private.h"
#include "platform.h"

#define _BUFFER_DEBUG 0

void _alInitBuffer(Buffer* buffer) {
    buffer->name = 0;
    buffer->is_dead = false;

    buffer->format = 0;
    buffer->data_size = 0;
    buffer->data = 0;
    buffer->freq = 0;
}

AL_API void AL_APIENTRY alGenBuffers(ALsizei n, ALuint *buffers) {
#if _BUFFER_DEBUG
    printf("Generating %d buffers\n", n);
#endif

    Context* ctx = _alContext();
    if(!ctx) {
        _alSetError(AL_INVALID_OPERATION, "No current context");
        return;
    }

    ALsizei available = MAX_BUFFERS_PER_CONTEXT - ctx->buffer_count;
    if(n > available) {
        _alSetError(AL_OUT_OF_MEMORY, "Not enough buffers");
        return;
    }

    Buffer* s = ctx->buffers;
    ALsizei c = 0;
    for(int i = 0; i < MAX_BUFFERS_PER_CONTEXT; ++i) {
        if(s[i].is_dead) {
            buffers[c++] = i + 1;

            /* Make sure we start from a clean slate */
            _alInitBuffer(&s[i]);
            s[i].is_dead = false;
            s[i].name = i + 1;

            if(c == n) {
                break;
            }
        }
    }

    assert(c == n);
}

AL_API void AL_APIENTRY alDeleteBuffers(ALsizei n, const ALuint *buffers) {
#if _BUFFER_DEBUG
    printf("Deleting %d buffers\n", n);
#endif

    Context* ctx = _alContext();
    if(!ctx) {
        _alSetError(AL_INVALID_OPERATION, "No current context");
        return;
    }

    for(int i = 0; i < n; ++i) {
        Buffer* s = &ctx->buffers[buffers[i] - 1];
        if(s->is_dead) {
            _alSetError(AL_INVALID_NAME, "Tried to delete an invalid source");
        }
    }

    for(int i = 0; i < n; ++i) {
        Buffer* s = &ctx->buffers[buffers[i] - 1];
        s->is_dead = true;

        if(s->data) {
            free_spu_ram(s->data);
        }
    }
}

AL_API ALboolean AL_APIENTRY alIsBuffer(ALuint buffer) {
    return (_alBuffer(buffer)) ? AL_FALSE : AL_TRUE;
}

AL_API void AL_APIENTRY alBufferData(ALuint buffer, ALenum format, const ALvoid *data, ALsizei size, ALsizei freq) {
    const ALenum VALID [] = {
        AL_FORMAT_MONO8,
        AL_FORMAT_MONO16,
        AL_FORMAT_STEREO8,
        AL_FORMAT_STEREO16,
        0
    };

    if(!_alIsValidEnum(VALID, format)) {
        _alSetError(AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    if(freq == 48000) {
        _alSetError(AL_INVALID_VALUE, "Dreamcast doesn't support 48k audio");
        return;
    }

    bool is_stereo = (format == AL_FORMAT_STEREO8) || (format == AL_FORMAT_STEREO16);

    if(is_stereo && (size % 2) != 0) {
        _alSetError(AL_INVALID_VALUE, "Odd number of bytes uploaded to stereo buffer");
        return;
    }

#if _BUFFER_DEBUG
    printf("Uploading %d bytes to buffer %d\n", size, buffer);
#endif

    Buffer* obj = _alBuffer(buffer);
    if(!obj) {
        _alSetError(AL_INVALID_NAME, "Not a valid buffer");
        return;
    }

    if(obj->data) {
        free_spu_ram(obj->data);
    }

    obj->data = alloc_spu_ram(size);

    if(is_stereo) {
        /* Create a temporary buffer to separate the data */
        uint8_t* tmp = (uint8_t*) malloc(size);
        uint8_t stride = (format == AL_FORMAT_STEREO8) ? 1 : 2;
        uint8_t* src0 = (uint8_t*) data;
        uint8_t* src1 = src0 + stride;
        uint8_t* dst0 = tmp;
        uint8_t* dst1 = tmp + (size / 2); /* 2 channels */

        for(int i = 0; i < size / 2; i += stride) {
            for(int j = 0; j < stride; ++j) {
                *dst0 = *src0;
                *dst1 = *src1;

                dst0++;
                dst1++;
                src0++;
                src1++;
            }

            src0 += stride;
            src1 += stride;
        }

        copy_to_spu(obj->data, tmp, size);
        free(tmp);
    } else {
        copy_to_spu(obj->data, (void*) data, size);
    }

    if(!obj->data) {
        _alSetError(AL_OUT_OF_MEMORY, "Ran out of memory :(");
        return;
    }

    obj->format = format;
    obj->data_size = size;
    obj->freq = freq;
}

AL_API void AL_APIENTRY alBufferf(ALuint buffer, ALenum param, ALfloat value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alBuffer3f(ALuint buffer, ALenum param, ALfloat value1, ALfloat value2, ALfloat value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alBufferfv(ALuint buffer, ALenum param, const ALfloat *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alBufferi(ALuint buffer, ALenum param, ALint value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alBuffer3i(ALuint buffer, ALenum param, ALint value1, ALint value2, ALint value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alBufferiv(ALuint buffer, ALenum param, const ALint *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetBufferf(ALuint buffer, ALenum param, ALfloat *value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetBuffer3f(ALuint buffer, ALenum param, ALfloat *value1, ALfloat *value2, ALfloat *value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetBufferfv(ALuint buffer, ALenum param, ALfloat *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetBufferi(ALuint buffer, ALenum param, ALint *value) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetBuffer3i(ALuint buffer, ALenum param, ALint *value1, ALint *value2, ALint *value3) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetBufferiv(ALuint buffer, ALenum param, ALint *values) {
    _alSetError(AL_INVALID_OPERATION, "Not Implemented");
}

Buffer* _alBuffer(ALuint buffer) {
    Context* ctx = _alContext();
    if(!ctx) {
        return NULL;
    }

    assert(buffer > 0);
    assert(buffer < MAX_BUFFERS_PER_CONTEXT);

    Buffer* b = &ctx->buffers[buffer - 1];
    if(b->is_dead) {
        return NULL;
    }

    return b;
}
